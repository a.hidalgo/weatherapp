import React from 'react';
import WeartherTemperature from './WeatherTemperature';
import WeartherExtraInfo from './WeatherExtraInfo';

const WeartherData = () => {
    return (
        <div>
            <WeartherTemperature />
            <WeartherExtraInfo humidity="80" wind="10m/s"/>
        </div>
    );
};

export default WeartherData;
