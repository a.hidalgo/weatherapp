import React from 'react';

/**
 * Example using destructuring and literals
 * 
 * @param string humidity
 * @param string wind
 */
const WeatherExtraInfo = ({ humidity, wind }) => {
    return (
        <div>
            <span>{`${humidity} % | ` }</span>
            <span>{ `${wind} wind` }</span>
        </div>
    );
};

export default WeatherExtraInfo;
