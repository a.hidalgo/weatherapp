import React from 'react';
import WeatherIcons from 'react-weathericons';

const WeatherTemperature = () => {
    return (
        <div>
            <WeatherIcons name="sleet" size="2x" />
            <span>30 °C</span>
        </div>
    );
};

export default WeatherTemperature;
